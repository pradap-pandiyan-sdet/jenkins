package parallelTest;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

public class CrossBrowserScript {

    private WebDriver driver;

    /**
     * This function will execute before each Test tag in testng.xml

     */
    @BeforeTest
    @Parameters("browser")
    public void setup(String browser) throws Exception{
        //Check if parameter passed from TestNG is 'firefox'
        if(browser.equalsIgnoreCase("firefox")){
            //create firefox instance
            System.setProperty("webdriver.gecko.driver", "C:\\My documents\\Files\\apkfiles\\geckodriver.exe");
            driver = new FirefoxDriver();
        }
        //Check if parameter passed as 'chrome'
        else if(browser.equalsIgnoreCase("chrome")){
            //set path to chromedriver.exe
             //create chrome instance
            driver = new ChromeDriver();
        }
        //Check if parameter passed as 'Edge'
        else if(browser.equalsIgnoreCase("Edge")){
            //set path to Edge.exe
            System.setProperty("webdriver.ie.driver","C:\\My documents\\Files\\apkfiles\\IEDriverServer.exe");
            //create Edge instance
            driver = new InternetExplorerDriver();
        }
        else{
            //If no browser passed throw exception
            throw new Exception("Browser is not correct");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


    }

    @Test
    public void testParameterWithXML() {
        driver.get("https://www.google.com/");
       // takescreenshot("urloaded" );
        //Find user name
        WebElement userName = driver.findElement(By.name("q"));
        //Fill user name
        userName.sendKeys("google");
        //Find password
    /*    WebElement password = driver.findElement(By.name("password"));
        //Fill password
        password.sendKeys("guru99");*/
        LocalDateTime myObj = LocalDateTime.now();
        takescreenshot("screenshottest"+myObj );

        Actions actions = new Actions(driver);
        WebElement elementLocator = driver.findElement(By.name("q"));
        actions.contextClick(elementLocator).perform();
    }

    private void takescreenshot(String screenshotname)
    {
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
// Now you can do whatever you need to do with it, for example copy somewhere
        try {
            FileUtils.copyFile(scrFile, new File("C:\\My documents\\Files\\Screenshots\\"+screenshotname+"screenshot.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @AfterTest
    public void AfterTest()
    {

         driver.close();
    }
}
